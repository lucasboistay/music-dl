# music-dl
Just a small script for downloading music from spotify or soundcloud with python

## Requirements

### [Python](https://www.python.org/downloads/)

Just download the latest stable Python and install it.

### [Pip](https://pip.pypa.io/en/stable/installation/)

Just use the command 'python -m ensurepip --upgrade' to install pip

### [FFmpeg](https://ffmpeg.org/download.html)

Download the latest static build for your OS and add the bin folder to your PATH environment variable.

## Installation

```bash
git clone https://github.com/lucasboistay/music-dl.git
cd music-dl
pip install -r requirements.txt
```

### SoundCloud OAuth token

To be able to connect your SoundCloud account to DL better quality music, find your OAuth token by visiting SoundCloud after logging in and opening developer console (press F12) and going to the Storage tab. Then under cookies > soundcloud.com you can find the entry called oauth_token.

You can then add this in the `~/.config/scdl/scdl.cfg` file.

Even without Go+, you can sometime download good quality songs. But having Go+ let you have m4a songs all the time.

## Usage

For SoundCloud :

First you need to download the songs with the archive to save the list of songs :

```bash
scdl -l [playlist_link] --force-metadata --path [path_to_music_folder] --flac --download-archive [path_to_playlist_folder/archive.txt] 
``` with the path_to_playlist_folder being just the path to the music folder + the name of the playlist (ex : ~/Music/ + EDM = ~/Music/EDM).

Then, when you want to sync your musics, just do :
 
```bash
scdl -l [playlist_link] --force-metadata --path [path_to_music_folder] --flac --sync [path_to_playlist_folder/archive.txt]
```

For Spotify :
```bash

```
